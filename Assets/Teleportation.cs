﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Teleport : MonoBehaviour
{
    public GameObject ObjectToTeleport;
    public float X;
    public float Y;
    public float Z;

    public void OnTriggerStay(Collider Player)
    {
    if(Player.tag == "Player")
    {
    Debug.Log("Player in trigger");
    ObjectToTeleport.transform.position = new Vector3(X,Y,Z);
    Debug.Log("Player moved");
    }
    }

    public Transform teleportTarget;
    public GameObject thePlayer;

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Player in trigger");
        thePlayer.transform.position = teleportTarget.transform.position;
        Debug.Log("Player moved");
    }

}
