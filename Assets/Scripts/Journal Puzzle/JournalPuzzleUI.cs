﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JournalPuzzleUI : MonoBehaviour
{
    public Canvas EPrompt;
    public Canvas JournalPuzzle;
    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider Player)
    {
        if (Player.tag == "Player")
        {
            Debug.Log("enter");
            EPrompt.enabled = true;
        }
    }

    void OnTriggerExit(Collider Player)
    {
        if (Player.tag == "Player")
        {
            Debug.Log("leave");
            EPrompt.enabled = false;
            JournalPuzzle.enabled = false;
        }
    }

    void OnTriggerStay(Collider Player)
    {
        Debug.Log("stay");
        if (Player.tag == "Player")
        {
            Debug.Log("player is in the trigger");
            //show the puzzle when we press 'E'
            if (Input.GetKeyDown(KeyCode.E))
            {
                JournalPuzzle.enabled = true;
                EPrompt.enabled = false;
                //show the cursor
                Cursor.lockState = CursorLockMode.None;
            }

            else if (Input.GetKeyDown(KeyCode.Escape))
            //hides (exits) the puzzle when we press 'Esc'
            {
                JournalPuzzle.enabled = false;
                EPrompt.enabled = true;
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
    }


}