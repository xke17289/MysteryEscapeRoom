﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Show_Text : MonoBehaviour
{
    public Canvas PlainText;
  
    public void ViewText()
    {
        Debug.Log("Showing Text");
        PlainText.enabled = true;
    }
}
