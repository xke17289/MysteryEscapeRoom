﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Gives us access to Unity's built in UI tools

public class ShowUI : MonoBehaviour
{
    public Canvas overlayScreen;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider Player)
    {
        if (Player.tag == "Player")
        {
            //show the canvas / hide the UI canvas
            overlayScreen.enabled = true;
        }
    }

    void OnTriggerExit(Collider Player)
    {
        if (Player.tag == "Player")
        {
            //hide the canvas / show the UI canvas
            overlayScreen.enabled = false;
        }
    }
}
